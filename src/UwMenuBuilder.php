<?php

namespace Drupal\uw_menu;

use Drupal\Core\Menu\MenuTreeParameters;

class UwMenuBuilder {

  /**
   * Get menu tree uw_menu.
   *
   * @param string $menu_name
   *   Public static function getMenuTree menu_name.
   * @param array $items
   *   Public static function getMenuTree items.
   * @param int $level
   *   Public static function getMenuTree level.
   *
   * @return array
   *   Public static function getMenuTree array.
   */
  public static function getMenuTree($menu_name, $items = [], $level = 0) {

    // Variable for results.
    $result = [];

    // If we are on the first level of the menu, add attributes.
    // If we are on anything but the first level, need different attributes.
    if ($level == 0) {

      // Get the active trail ids use Drupal core.
      $menu_active_trail = \Drupal::service('menu.active_trail')->getActiveTrailIds($menu_name);
      $menu_tree_parameters = (new MenuTreeParameters)->setActiveTrail($menu_active_trail)->onlyEnabledLinks();

      // Get the menu tree.
      $tree = \Drupal::menuTree()->load($menu_name, $menu_tree_parameters);

      // Step through each item in the tree, and setup the variables.
      foreach ($tree as $item) {

        // Get the route name.
        $route_name = $item->link->getPluginDefinition()['route_name'];

        // Variables to add.
        $result[] = [
          'title' => $item->link->getTitle(),
          'level' => $level,
          'description' => $item->link->getDescription(),
          'weight' => $item->link->getWeight(),
          'url' => $item->link->getUrlObject()->toString(),
          'subtree' => self::getMenuTree($menu_name, $item, $level + 1),
          'route_name' => $route_name,
          'in_active_trail' => $item->inActiveTrail,
        ];
      }
    }
    else {

      // If the item has children, process them.
      if ($items->hasChildren) {

        // Step through each of the subtrees and see the variables.
        foreach ($items->subtree as $key_item => $item) {

          // Get the route name.
          $route_name = $item->link->getPluginDefinition()['route_name'];

          // Variables to add.
          $result[] = [
            'title' => $item->link->getTitle(),
            'level' => $level,
            'description' => $item->link->getDescription(),
            'weight' => $item->link->getWeight(),
            'url' => $item->link->getUrlObject()->toString(),
            'subtree' => self::getMenuTree($menu_name, $item, $level + 1),
            'route_name' => $route_name,
            'in_active_trail' => $item->inActiveTrail,
          ];
        }
      }

    }

    // Return the results.
    return $result;
  }

  /**
   * Get menu tree sorted by weight ascending.
   *
   * @param string $menu_name
   *   Public static function getMenuTreeOrder menu_name.
   * @param array $items
   *   Public static function getMenuTreeOrder items.
   * @param int $level
   *   Public static function getMenuTreeOrder level.
   *
   * @return array
   *   Public static function getMenuTreeOrder array.
   */
  public static function getMenuTreeOrder($menu_name, $items = [], $level = 0) {

    // Load in the menu.
    $menu = self::getMenuTree($menu_name, $items = [], $level = 0);

    // Return the deep sorted menu (with all the submenus sorted).
    return self::sortMenuDeep($menu);
  }

  /**
   * Sort list child menu.
   *
   * @param string $menu
   *   Public static function sortMenuDeep menu.
   *
   * @return array
   *   Public static function sortMenuDeep array.
   */
  public static function sortMenuDeep($menu) {

    // If the menu is an array, continue to process.
    if (is_array($menu)) {

      // Sort the menu based on the items.
      $menu = self::sortMenu($menu);

      // Step through each of the menu items and sort it.
      foreach ($menu as $key_item => $item) {

        // If the item has a subtree, sort that subtree.
        if (isset($item['subtree'])) {

          // Sort the subtree menu.
          $menu[$key_item]['subtree'] = self::sortMenuDeep($item['subtree']);
        }
      }

      // Return the menu.
      return $menu;
    }
    return [];
  }

  /**
   * Sort menu by weight.
   *
   * @param string $menu
   *   Public static function sortMenu string menu.
   *
   * @return array
   *   Public static function sortMenu array.
   */
  public static function sortMenu($menu) {

    // Steo through the menu and sort by weight.
    for ($i = 0; $i < count($menu); $i++) {

      // Step through each of menus again and sort based on weight.
      for ($j = $i + 1; $j < count($menu); $j++) {

        // If the previous menu has a large weight, swap them.
        if ($menu[$i]['weight'] > $menu[$j]['weight']) {

          // Perform the swap of the items.
          $menu_tmp = $menu[$i];
          $menu[$i] = $menu[$j];
          $menu[$j] = $menu_tmp;
        }
      }
    }

    // Return the menu.
    return $menu;
  }
}